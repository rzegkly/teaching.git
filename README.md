# 挑战三  Mind绘制抛物线教案

#### 介绍
【终极挑战 教学分享】挑战三  Mind+绘制抛物线教案
篮球运动的轨迹就是一条抛物线，那么，我们如何绘制抛物线?我们可以通过数学中的描点法来实现！
在数学中，通过函数图像表示抛物线的，举个例子：y=- x*x,数学课代表给大家分享绘制的过程，1、绘制坐标轴； 2、根据函数表达式，取点； 3、描点，并连线。其实，线由点组成，点足够多，就构成了线 如果取10个点，20个点，很容易！那么我们取1万点，你觉得人工取点可行？ 那么，我们用计算机帮助我们。 如何使用计算机编写程序来解决这个问题, 今天我们一起学习《信息编程加工——Mind+绘制抛物线》! 

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)