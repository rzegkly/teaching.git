# 挑战三  Mind绘制抛物线教案

#### Description
【终极挑战 教学分享】挑战三  Mind+绘制抛物线教案
篮球运动的轨迹就是一条抛物线，那么，我们如何绘制抛物线?我们可以通过数学中的描点法来实现！
在数学中，通过函数图像表示抛物线的，举个例子：y=- x*x,数学课代表给大家分享绘制的过程，1、绘制坐标轴； 2、根据函数表达式，取点； 3、描点，并连线。其实，线由点组成，点足够多，就构成了线 如果取10个点，20个点，很容易！那么我们取1万点，你觉得人工取点可行？ 那么，我们用计算机帮助我们。 如何使用计算机编写程序来解决这个问题, 今天我们一起学习《信息编程加工——Mind+绘制抛物线》! 

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)